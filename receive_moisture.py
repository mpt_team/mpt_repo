#!/usr/bin/python3

import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
import time
from adafruit_mcp3xxx.analog_in import AnalogIn

spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
cs = digitalio.DigitalInOut(board.D5)
mcp = MCP.MCP3008(spi, cs)

channel = AnalogIn(mcp, MCP.P0)

# on startup clear history file
fd0 = open('/home/pi/mpt_project/humidity_hist', 'w')
fd0.close()

counter = 0
while True:
    with open('/home/pi/mpt_project/humidity_now', 'w') as fd1:
        humid_now = channel.value/65536*100
        fd1.write('{}\n'.format(humid_now))
        # each 5 minutes write to history file
        if counter % 150 == 0:
            with open('/home/pi/mpt_project/humidity_hist', 'a') as fd2:
                fd2.write('{}\n'.format(humid_now))
                counter = 0
    counter += 1
    time.sleep(2)
