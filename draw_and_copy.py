#!/usr/bin/python

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
import datetime
from shutil import copy

curr_time = datetime.datetime.now()

# read humidity
fd = open('/home/pi/mpt_project/humidity_hist', 'r')
humidities = fd.read().split('\n')
fd.close()

humidities = humidities[:-1]
humidities = [int(float(i)) for i in humidities]

# if measure lasts longer than 12 hours take humidity only from last 12 hours
if len(humidities) > 144:
    humidities = humidities[::-1]
    humidities = humidities[:144]
    humidities = humidities[::-1]

times = [curr_time - datetime.timedelta(minutes=i*5) for i in range(len(humidities)-1, -1, -1)]
plt.plot(times, humidities)
plt.ylim(0, 100)
plt.xlabel('time')
plt.ylabel('humidity [%]')
# format date
formatter = DateFormatter('%H:%M')
plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
plt.savefig('/home/pi/mpt_project/moisture_hist')

copy('/home/pi/mpt_project/moisture_hist.png', '/var/www/html/')