#!/usr/bin/python3

import sys
import os
import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)

level = int(sys.argv[1])
if not (0 < level < 10):
    sys.exit(1)

GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)

level *= 10
while True:
    with open('humidity_now', 'r') as fd:
        value = fd.readlines()
    humidity = value[0].strip()
    humidity = float(humidity)
    time.sleep(0.5)
    if (humidity < level):
        try:
            GPIO.output(21, GPIO.HIGH)
            time.sleep(0.5)
        finally:
            GPIO.output(21, GPIO.LOW)
